( function( $ ) {

	$( document ).ready( function() {

		var templates = [];
		var gc = $('#graph-cont');
		var glab = $('#graph-label');
		var grcn = $('#graph-cnt');
		var gtl = d3.select('.selectors');
		var cs = 0;

		$.ajax({
			url: "graph-temps",
			success: function(data){
				$(data).find("a:contains(.)").each(function(){
					var cur = $(this).attr("href");
					var plc = templates.length;
					gtl.append('a').attr('class', 'gsel'+plc)
						.on('click', function(){
							cs = plc;
							loadNew();
						})
						.on('mouseenter', function(){
							var y = this.offsetTop + 25;
							var x = this.offsetLeft;
							var w = window.innerWidth - 150;
							var j = "left";
							var s = "right";
							if( x > w ){
								j = "right";
								s = "left";
								x = 5;
							}

							d3.select("#graph-cnt h4")
								.style(s, "")
								.style(j, x+"px")
								.style("top", y+"px")
								.text(cur.replace('.html', '').split('_').join(" "));
						})
						.on('mouseleave', function(){
							d3.select("#graph-cnt h4").text("");
						});
					templates.push(cur);
				});

				loadNew();

				document.onkeydown = checkKey;

				function checkKey(e) {
				    e = e || window.event;

				    if (e.keyCode == '37') {
						if(cs <= 0){
							cs = templates.length - 1;
						}else{
							cs -= 1;
						}
						loadNew();
				    }
				    else if (e.keyCode == '39') {
						if(cs >= templates.length - 1){
							cs = 0;
						}else{
							cs += 1;
						}
						loadNew();
				    }

				}


				function loadNew(){
					$.ajax({
					  url: "graph-temps/"+templates[cs],
					  success: function(data) {
					    data = data.split("chartContainer").join("chartContainer"+cs);
					    $('article .graphs .graph > div').addClass('remove');
					    setTimeout(function(){
					    	d3.select('.selectors a.cur').classed('cur', false);
					    	d3.select('.selectors a.gsel'+cs).classed('cur', true);
						    gc.html(data);
						    glab.text((cs+1)+ ".) " +templates[cs].replace('.html', '').split('_').join(" "));
						}, 750);
					  }
					});
				}

			}
		});

	} );

} )( jQuery );